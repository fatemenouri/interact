var clients = [
    {
        img: "http://www.templatewire.com/preview/interact/img/testimonials/01.jpg",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.",
        name: "- John Doe",

    },
    {
        img: "http://www.templatewire.com/preview/interact/img/testimonials/02.jpg",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis.",
        name: "- Johnathan Doe",

    },
    {
        img: "http://www.templatewire.com/preview/interact/img/testimonials/03.jpg",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.",
        name: "- John Doe",

    },
    {
        img: "http://www.templatewire.com/preview/interact/img/testimonials/04.jpg",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis.",
        name: "- Johnathan Doe",

    },
    {
        img: "http://www.templatewire.com/preview/interact/img/testimonials/05.jpg",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.",
        name: "- John Doe",

    },
    {
        img: "http://www.templatewire.com/preview/interact/img/testimonials/06.jpg",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis.",
        name: "- Johnathan Doe",

    },
]
var  content_name, parent, element,imagediv,contentdiv,image;
parent = document.getElementById('parent');

for (var i = 0; i < clients.length; i++) {
   
    element = document.createElement('div');
    element.className = 'card' ;

    imagediv=document.createElement('div');
        imagediv.className='client-img';
        image=document.createElement('img');
        image.src=clients[i].img;

    contentdiv=document.createElement('div');
    contentdiv.className='client-content';
        content_name=document.createElement('div');
        content_name.textContent=clients[i].name;
        content_name.className='client_name';

        content_text=document.createElement('p');
        node=document.createTextNode(clients[i].content);
        content_text.appendChild(node);


    
    parent.appendChild(element);
    element.appendChild(imagediv);
    element.appendChild(contentdiv);
    imagediv.appendChild(image);
    contentdiv.appendChild(content_text);
    contentdiv.appendChild(content_name);
    
    
}